package br.com.workana.services;

import br.com.workana.Entities.Login;
import br.com.workana.interfaces.CadastroVerificacao;
import br.com.workana.repositories.LoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LoginService {

    @Autowired
    LoginRepository loginRepository;



    public void saveLogin (String nickName, String password) {
        Login usuarioCadastro = new Login();
        usuarioCadastro.setNickName(nickName);
        usuarioCadastro.setPassword(password);
        loginRepository.save(usuarioCadastro);
    }
}