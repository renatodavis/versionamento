package br.com.workana.services;

import br.com.workana.Entities.Cadastro;
import br.com.workana.repositories.CadastroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class CadastroService  {

    @Autowired
    CadastroRepository cadastroRepository;

    public Cadastro saveCadastro(Cadastro cadastro) {
            return cadastroRepository.save(cadastro);
    }

    public List<Cadastro> gettingCadastro() {
        return cadastroRepository.findAll();
    }

//    public Cadastro findByCadastro(String email, String password) {
//      return cadastroRepository.findByLogin(email, password);
//    }

    private Collection<GrantedAuthority> getGrantedAuthority(Cadastro cadastro) {
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
        return authorities;
    }

    public Cadastro loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Cadastro> cadastro = Optional.ofNullable(cadastroRepository.findByLogin(username));
        if (cadastro.isPresent()) {
            return cadastro.get();
        }

        throw new UsernameNotFoundException("Dados inválidos!");
    }
}