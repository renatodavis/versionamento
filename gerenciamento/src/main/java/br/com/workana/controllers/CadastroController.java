package br.com.workana.controllers;

import br.com.workana.Entities.Cadastro;
import br.com.workana.services.CadastroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class CadastroController {

    @Autowired
    CadastroService cadastroService;

    @PostMapping("/register")
    public void sendingRegister(@RequestBody Cadastro cadastro) {
        cadastroService.saveCadastro(cadastro);
    }

    @PostMapping("/login")
    public void findRegister(@RequestBody Cadastro cadastro) {
       cadastroService.loadUserByUsername(cadastro.getEmail());

    }
}
