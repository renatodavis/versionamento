package br.com.workana.repositories;

import br.com.workana.Entities.Cadastro;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CadastroRepository extends JpaRepository<Cadastro, Long> {
    @Query(value = "select * from cadastro where email = :email", nativeQuery = true)
    Cadastro findByLogin(String email);
}