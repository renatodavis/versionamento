package br.com.workana.repositories;

import br.com.workana.Entities.Login;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

public interface LoginRepository extends CrudRepository<Login, Long>, JpaSpecificationExecutor<Login> {

}