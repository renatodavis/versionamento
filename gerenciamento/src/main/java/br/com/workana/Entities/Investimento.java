package br.com.workana.Entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "investimento")
@Getter
@Setter
public class Investimento {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Integer investimento;

    public Investimento() {
    }

    public Investimento(Investimento investimento) {
    }
}
